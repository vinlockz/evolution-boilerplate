FROM node:11.12.0

WORKDIR /var/app

COPY package*.json ./
COPY yarn.lock ./
COPY .npmrc ./
COPY .yarnrc ./
COPY pm2.dev.json ./

RUN npm install yarn -g

RUN npm install pm2 -g

RUN yarn install

COPY dist/ ./dist/

COPY .env.prod ./.env

CMD [ "pm2-runtime", "pm2.dev.json" ]
