/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import jwt from 'jsonwebtoken';
import moment from 'moment';
import FormValidations from '../../lib/FormValidations';
import { stringsEqual } from '../../../utils/helpers';

/** ENVIRONMENT VARIABLES */
const { ADMIN_JWT_SECRET } = process.env;

export default {
  logout: async (req, res) => {
    res.clearCookie('token');
    res.json({ success: true });
  },
  login: async (req, res) => {
    const { email, password } = req.body;

    const user = await req.db.User.findOne({ email }).exec();
    if (!user) {
      return res.json({ error: 'INVALID_EMAIL' }).status(403);
    }

    if (!user.enabled) {
      return res.json({ error: 'USER_DISABLED' }).status(403);
    }

    if (!user.comparePassword(password)) {
      return res.json({ error: 'INVALID_PASSWORD' }).status(403);
    }

    const jwtToken = jwt.sign(user.toObject(), ADMIN_JWT_SECRET);

    const expires = moment().add(1, 'M').toDate();
    res.cookie('token', jwtToken, { expires, httpOnly: true });
    return res.json({ success: true });
  },
  create: async (req, res) => {
    const {
      email, name, password, confirmPassword,
    } = req.body;

    if (FormValidations.isEmail(email) !== undefined) {
      return res.json({ error: 'INVALID_EMAIL' }).status(400);
    }

    console.log(password);
    if (!stringsEqual(password, confirmPassword)) {
      return res.json({ error: 'PASSWORD_NO_MATCH' }).status(400);
    }

    const currentUser = await req.db.User.findOne({ email }).exec();

    if (currentUser) {
      return res.json({ error: 'USER_EXISTS' }).status(400);
    }

    const userCount = await req.db.User.countDocuments().exec();
    const hasUsers = Boolean(userCount);
    try {
      const newUser = await req.db.User.createAccount({
        email, name, password, enabled: !hasUsers, first: !hasUsers,
      });
      if (!hasUsers) {
        const jwtToken = jwt.sign(newUser.toObject(), ADMIN_JWT_SECRET);
        res.cookie('token', jwtToken, { maxAge: 900000, httpOnly: true });
        return res.json(newUser);
      }
    } catch (err) {
      return res.json({ error: err.message }).status(500);
    }
    return res.json({ success: true });
  },
  updatePassword: async (req, res) => {
    req.user.password = req.body.password;
    req.user.save(() => {
      res.json({ success: true });
    });
  },
  updateEmail: async (req, res) => {
    req.user.email = req.body.email;
    req.user.save(() => {
      res.json({ success: true });
    });
  },
  updateName: async (req, res) => {
    req.user.name = req.body.name;
    req.user.save(() => {
      res.json({ success: true });
    });
  },
};
