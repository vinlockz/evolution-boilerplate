/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import express from 'express';
import controllers from './controllers';
import middleware from '../../lib/ExpressMiddleware';

const { ADMIN_KEY } = process.env;

const secureEndpointMiddleware = () => (req, res, next) => {
  if (req.user || req.headers['x-admin-key'] === ADMIN_KEY) {
    return next();
  } else {
    return res.status(403).json({
      error: 'ACCESS_DENIED',
    });
  }
};

/** API Router */
const apiRouter = express.Router();

/** MIDDLEWARE */
apiRouter.use(middleware({
  jsonParser: true,
  requestIdGenerator: true,
  logging: { name: 'adminApi' },
}));

// Auth Router
const authRouter = express.Router();
authRouter.post('/login', controllers.authController.login);
authRouter.post('/logout', controllers.authController.logout);
authRouter.put('/create', controllers.authController.create);
authRouter.post('/user/password', controllers.authController.updatePassword);
authRouter.post('/user/name', controllers.authController.updateName);
authRouter.post('/user/email', controllers.authController.updateEmail);
apiRouter.use('/auth', authRouter);

/** Settings Router */
const settingsRouter = express.Router();
settingsRouter.use(secureEndpointMiddleware());
settingsRouter.get('/:key', controllers.settingsController.get);
settingsRouter.post('/:key', controllers.settingsController.set);
apiRouter.use('/settings', settingsRouter);

/** User Router */
const userRouter = express.Router();
userRouter.use(secureEndpointMiddleware());
userRouter.delete('/:id', controllers.userController.delete);
userRouter.post('/:id/approve', controllers.userController.approve);
userRouter.post('/:id/disable', controllers.userController.disable);
apiRouter.use('/users', userRouter);

export default apiRouter;
