/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import React, { Component } from 'react';
import classnames from 'classnames';
import Head from 'next/head';
import PropTypes from 'prop-types';
import './PageContainer.css';

class PageContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      copied: false,
    };
  }

  copy = (text) => () => {
    // Create new element
    const el = document.createElement('textarea');
    // Set value (string to be copied)
    el.value = text;
    // Set non-editable to avoid focus and move outside of view
    el.setAttribute('readonly', '');
    el.style = {
      position: 'absolute',
      left: '-9999px',
    };
    document.body.appendChild(el);
    // Select text inside element
    el.select();
    // Copy text to clipboard
    document.execCommand('copy');
    // Remove temporary element
    document.body.removeChild(el);

    this.setState({ copied: true }, () => {
      setTimeout(() => {
        this.setState({ copied: false });
      }, 1000);
    });
  };

  componentDidMount() {
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });
  };

  render() {
    const { copied } = this.state;
    const { children, title, pageTitle, icon, settingKey } = this.props;

    const codeClasses = classnames({
      pageContainerCode: true,
      rounded: true,
      pageContainerCodeSelected: copied,
    });

    return (
      <div className="container">
        <Head>
          <title>{pageTitle}</title>
        </Head>
        <section className="border shadow p-3 mb-5 bg-white rounded">
          <h2>
            <i className={icon} /> {title}
            <small className={classnames({ 'd-none': !settingKey })}>
              <code id="code" className={codeClasses} onClick={this.copy(settingKey)}>
                {settingKey}
              </code>
              <i className="fas fa-copy copy" />
            </small>
          </h2>
          <hr className="border" />
          {children}
          <hr className="border" />
        </section>
      </div>
    )
  }
}

PageContainer.defaultProps = {
  pageTitle: 'ZephyrQL Admin',
  settingkey: null,
};

PageContainer.propTypes = {
  pageTitle: PropTypes.string,
  title: PropTypes.string.isRequired,
  settingKey: PropTypes.string,
  icon: PropTypes.string.isRequired,
};

export default PageContainer;
