/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import express from 'express';
import next from 'next';
import helmet from 'helmet';
import middleware from '../lib/ExpressMiddleware';
import adminMiddleware from './lib/ExpressMiddleware';
import apiRouter from './api';

const {
  NEXT_ENV,
  DISABLE_HELMET,
  DISABLE_HELMET_HSTS,
  DISABLE_HELMET_CACHE_CONTROL,
  DISABLE_HELMET_CROSS_DOMAIN,
  DISABLE_HELMET_HIDE_POWERED_BY,
} = process.env;

const runServer = (port) => {
  const dev = NEXT_ENV !== 'production';
  const app = next({
    dev,
    dir: __dirname,
    quiet: false,
  });
  const handle = app.getRequestHandler();

  return app.prepare().then(() => {
    // Instantiate Server
    const server = express();

    // Helmet Middleware
    if (DISABLE_HELMET !== 'true') {
      if (DISABLE_HELMET_HSTS !== 'true') {
        server.use(helmet.hsts());
      }
      if (DISABLE_HELMET_CACHE_CONTROL !== 'true') {
        server.use(helmet.noCache());
      }
      if (DISABLE_HELMET_CROSS_DOMAIN !== 'true') {
        server.use(helmet.permittedCrossDomainPolicies());
      }
      if (DISABLE_HELMET_HIDE_POWERED_BY !== 'true') {
        server.use(helmet.hidePoweredBy());
      }
    }

    server.get('/test', (req, res) => res.status(200).json({ success: true }));

    server.use(middleware({ cookieParser: true }));

    // Admin Middleware
    server.use(adminMiddleware({
      adminDb: true,
      adminAuth: true,
    }));

    // Static Files
    server.use('/static', express.static('app/admin/static'));

    // API
    server.use('/api', apiRouter);

    // Admin Handler
    server.get('*', (req, res) => handle(req, res));

    server.listen(port, (err) => {
      if (err) throw err;
      console.log(`Server Ready on ${port}`);
    });
  }).catch((error) => {
    throw error;
  });
};

export default { runServer };
