/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const isEmail = value => (/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/.test(value) ? undefined : 'Invalid E-Mail Address');
const minLength = min => value => (String(value).length > min ? undefined : `Minimum ${min} characters.`);
const matchesPassword = (value, allValues) => ((value === allValues.password) ? undefined : 'Passwords do not match.');
const required = value => (value ? undefined : 'Required.');
const composeValidators = (...validators) => (value, allValues, meta) => validators
  .reduce((error, validator) => error || validator(value, allValues, meta), undefined);
const maxLength = max => value => (String(value).length <= max ? undefined : `Maximum ${max} characters.`);

module.exports = {
  isEmail,
  minLength,
  maxLength,
  matchesPassword,
  required,
  composeValidators,
};
