/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import React from 'react';
import { CookiesProvider } from 'react-cookie';
import Document, { Head, Main, NextScript } from 'next/document';
import './core.css';

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = {};
    const { req, res } = ctx;
    const redirect = `http://${req.headers.host}/auth/login`;
    if (!req.user && req.url !== '/auth/login' && req.url !== '/auth/create') {
      if (res && req) {
        res.writeHead(302, {
          Location: redirect,
        });
        res.end();
      } else if (typeof window === 'object') {
        window.location = redirect;
      }
    }

    if (req.user) {
      initialProps.user = req.user;
    }

    return initialProps;
  }

  render() {
    const { user } = this.props;
    return (
      <html>
        <Head>
          <title>Admin CMS</title>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta name="robots" content="noindex,nofollow" />
          {/* Jquery */}
          <script src="/static/dependencies/jquery/jquery-3.3.1.min.js"></script>

          {/* Bootstrap */}
          <link rel="stylesheet" href="/static/dependencies/bootstrap/4.3.0/css/bootstrap.min.css" />
          <script src="/static/dependencies/bootstrap/4.3.0/js/bootstrap.bundle.min.js"></script>

          {/* FontAwesome */}
          <script src="/static/dependencies/fontawesome/5.6.3/js/all.min.js"></script>
        </Head>
        <body style={{backgroundColor: '#1F3263'}}>
          <CookiesProvider>
            <Main />
            <NextScript />
          </CookiesProvider>
        </body>
      </html>
    );
  }
};
