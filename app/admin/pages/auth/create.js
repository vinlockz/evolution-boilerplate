/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import React from 'react';
import Head from 'next/head';
import { Form, Field } from 'react-final-form';
import TextInput from '../../components/TextInput/TextInput';
import FormValidations from '../../lib/FormValidations';
import api from '../../lib/api';
import Modal from '../../components/Modal';

const { ADMIN_PANEL_TITLE } = process.env;

class Create extends React.Component {
  static async getInitialProps(ctx) {
    const { req, res } = ctx;
    const redirect = '/';

    if (req.user) {
      res.writeHead(302, { Location: redirect });
      res.end();
    }

    return {};
  }

  onSubmit = (values) => {
    api().put('/auth/create', {
      email: values.email,
      name: values.name,
      password: values.password,
      confirmPassword: values.confirmPassword,
    }).then((res) => {
      if (res.data.success) {
        // eslint-disable-next-line
        window.location.href = '/';
      } else {
        $('#createAccountError').modal('show');
      }
    });
    return false;
  };

  renderForm = ({ handleSubmit, pristine, invalid }) => (
    <form className={'col-lg-6 offset-lg-3 col-md-12'} onSubmit={handleSubmit}>
      <h2>Create Account</h2>
      <div className="row">
        <div className="col">
          <Field
            name="email"
            render={TextInput({ label: 'E-Mail Address' })}
            validate={FormValidations
              .composeValidators(
                FormValidations.required,
                FormValidations.isEmail,
              )}
          />
          <Field
            name="name"
            render={TextInput({ label: 'Name' })}
            validate={FormValidations
              .composeValidators(
                FormValidations.required,
                FormValidations.minLength(2),
              )}
          />
          <Field
            name={'password'}
            render={TextInput({ label: 'Password', password: true })}
            validate={FormValidations.composeValidators(
              FormValidations.required,
              FormValidations.minLength(8),
              FormValidations.maxLength(20),
            )}
           />
           <Field
             name={'confirmPassword'}
             render={TextInput({ label: 'Confirm Password', password: true })}
             validate={FormValidations.composeValidators(
               FormValidations.required,
               FormValidations.minLength(8),
               FormValidations.maxLength(20),
               FormValidations.matchesPassword,
             )}
           />
        </div>
      </div>
      <div className="d-flex flex-row-reverse">
        <button type="submit" disabled={pristine || invalid} className="btn btn-primary">Create Account</button>
      </div>
    </form>
  );

  render() {
    return (
      <div>
        <Head>
          <title>Create Account - {ADMIN_PANEL_TITLE}</title>
        </Head>
        <div className={'border shadow p-3 mb-5 bg-white rounded container text-center'}>
          <Form onSubmit={this.onSubmit} render={this.renderForm} />
          <Modal
            id={'createAccountError'}
            title={'Error'}
            body={'User Already Exists'}
            actions={[
              <button key={0} onClick={() => location.reload()} className="btn btn-primary">Ok</button>,
            ]}
           />
        </div>
      </div>
    );
  }
}

export default Create;
