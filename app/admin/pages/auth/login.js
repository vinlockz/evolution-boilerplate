/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import React from 'react';
import Head from 'next/head';
import { Form, Field } from 'react-final-form';
import api from '../../lib/api';
import TextInput from '../../components/TextInput';
import FormValidations from '../../lib/FormValidations';
import Modal from '../../components/Modal';

const { ADMIN_PANEL_TITLE } = process.env;

class Login extends React.Component {
  static async getInitialProps(ctx) {
    const { req, res } = ctx;
    const redirect = '/';
    const createAccountPath = '/auth/create';

    const userCount = await req.db.User.countDocuments().exec();
    const hasUsers = Boolean(userCount);

    if (req.user) {
      res.writeHead(302, { Location: redirect });
      res.end();
    } else if (!hasUsers) {
      res.writeHead(302, { Location: createAccountPath });
      res.end();
    }
    return {};
  }

  onSubmit = (values) => {
    api().post('/auth/login', {
      email: values.email,
      password: values.password,
    }).then((response) => {
      if (response.data.error) {
        $('#forbidden_modal').modal('show');
      } else {
        // eslint-disable-next-line
        window.location.href = '/';
      }
    });
  };

  renderForm = ({ handleSubmit, pristine, invalid }) => {
    return (
      <form className="col-lg-6 offset-lg-3 col-md-12" onSubmit={handleSubmit}>
        <h2>Login</h2>
        <div className="row">
          <div className="col">
            <Field
              name="email"
              render={TextInput({ label: 'E-Mail Address' })}
              validate={FormValidations
                .composeValidators(FormValidations.required, FormValidations.isEmail)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <Field
              name="password"
              render={TextInput({ label: 'Password', password: true })}
              validate={FormValidations
                .composeValidators(
                  FormValidations.required,
                  FormValidations.minLength(8),
                )}
            />
          </div>
        </div>
        <div className="d-flex flex-row-reverse">
          <div className="ml-auto">
            <button type="submit" disabled={pristine || invalid} className="btn btn-primary">Login</button>
          </div>
          <div>
            <a href={'/auth/create'} className="btn btn-link">Create Account</a>
          </div>
        </div>
      </form>
    );
  };

  render() {
    return (
      <div>
        <Head>
          <title>Login - {ADMIN_PANEL_TITLE}</title>
        </Head>
        <div className="border shadow p-3 mb-5 bg-white rounded container text-center">
          <Form onSubmit={this.onSubmit} render={this.renderForm} />
        </div>
        <Modal
          id="forbidden_modal"
          title="Error"
          body={(
            <div>
              <p>Invalid Login Information</p>
            </div>
          )}
          actions={[
            <button key={0} type="button" className="btn btn-primary" data-dismiss="modal">Close</button>,
          ]}
        />
      </div>
    );
  }
}

export default Login;
