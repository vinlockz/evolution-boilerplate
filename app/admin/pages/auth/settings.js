/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import React from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import { Form, Field } from 'react-final-form';
import PageContainer from '../../containers/PageContainer';
import Modal from '../../components/Modal';
import api from '../../lib/api';
import FormValidations from '../../lib/FormValidations';

// eslint-disable-next-line react/display-name,react/prop-types
const SettingsInput = ({ label, password, description }) => ({ input, meta }) => {
  if (password === undefined) password = false;
  const disabled = meta.invalid || !meta.modified || meta.error;
  return (
    <div className="form-group text-left">
      <div>
        <label htmlFor={input.name}>{label}</label>
        <small className="text-muted float-right">{description}</small>
      </div>
      <div className="input-group text-left">
        <input {...input} id={input.name} type={password ? 'password' : 'text'} className="form-control" placeholder={input.placeholder} aria-label={input.placeholder} aria-describedby={input.name} />
        <div className="input-group-append">
          <button className={classnames('btn', { 'btn-dark': disabled, 'btn-success': !disabled })} type="submit" disabled={disabled} id={`${input.name}_submit`}>Save</button>
        </div>
      </div>
      {meta.touched && meta.error && <small className="text-danger">{meta.error}</small>}
    </div>
  );
};

class Settings extends React.Component {
  constructor(props) {
    super(props);
    const user = _.clone(props.user);
    this.state = { user };
  }

  static async getInitialProps(ctx) {
    const initialProps = {
      user: null,
    };
    if (!process.browser) {
      const { req } = ctx;
      try {
        initialProps.user = req.user;
      } catch (err) {}
    }

    return initialProps;
  }

  renderPasswordForm = ({ handleSubmit, pristine, invalid }) => {
    const name = 'password';
    const render = SettingsInput({
      label: 'Password',
      password: true,
      description: 'Your password...',
    });
    const validate = FormValidations.composeValidators(
      FormValidations.required,
      FormValidations.minLength(8),
      FormValidations.maxLength(20),
    );
    const defaultValue = '******';
    return this.renderField({ name, render, validate, handleSubmit, defaultValue });
  };

  renderEmailForm = ({ handleSubmit, pristine, invalid }) => {
    const { email } = this.props.user;
    const name = 'email';
    const render = SettingsInput({
      label: 'E-Mail Address',
      description: 'Your e-mail address...',
    });
    const validate = FormValidations.composeValidators(
      FormValidations.required,
      FormValidations.isEmail,
    );
    const placeholder = 'E-Mail Address';
    const defaultValue = email;
    return this.renderField({ name, render, validate, placeholder, handleSubmit, defaultValue });
  };

  renderNameForm = ({ handleSubmit, pristine, invalid }) => {
    const { name: username } = this.props.user;

    const name = 'name';
    const render = SettingsInput({
      label: 'Name',
      description: 'Literally just your name...',
    });
    const validate = FormValidations.composeValidators(
      FormValidations.required,
      FormValidations.minLength(2),
    );
    const placeholder = 'Your name...';
    const defaultValue = username;
    return this.renderField({ name, render, validate, placeholder, handleSubmit, defaultValue });
  };

  renderField = ({ name, render, validate, placeholder, handleSubmit, defaultValue }) => {
    return (
      <form className="col-lg-6 offset-lg-3 col-md-12" onSubmit={handleSubmit}>
        <div className="row">
          <div className="col">
            <Field defaultValue={defaultValue} name={name} render={render} validate={validate} placeholder={placeholder} />
          </div>
        </div>
      </form>
    );
  };

  nameFormSubmit = (values) => {
    const { name } = values;
    api().post('/auth/user/name', { name })
      .then((response) => {
        if (response.data.success) {
          $('#name_change_confirm').modal('show');
        } else {
          $('#name_change_error').modal('show');
        }
      });
  };

  emailFormSubmit = (values) => {
    const { email } = values;
    api().post('/auth/user/email', { email })
      .then((response) => {
        if (response.data.success) {
          $('#email_change_confirm').modal('show');
        } else {
          $('#email_change_error').modal('show');
        }
      });
  };

  passwordFormSubmit = (values) => {
    const { password } = values;
    api().post('/auth/user/password', { password })
      .then((response) => {
        if (response.data.success) {
          $('#password_change_confirm').modal('show');
        } else {
          $('#password_change_error').modal('show');
        }
      });
  };

  modals = () => {
    const confirmModal = (type, key) => (
      <Modal
        key={key}
        id={`${type}_change_confirm`}
        title={`${type.charAt(0).toUpperCase() + type.slice(1)} Changed`}
        body={`Your ${type} change was successful`}
        actions={[
          <button key={0} type="button" className="btn btn-primary" onClick={() => location.reload()}>Ok</button>,
        ]}
      />
    );
    const errorModal = (type, key) => (
      <Modal
        key={key}
        id={`${type}_change_error`}
        title="Error"
        body={`Error changing ${type}`}
        actions={[
          <button key={0} type="button" className="btn btn-primary" onClick={() => location.reload()}>Ok</button>,
        ]}
      />
    );
    return [
      confirmModal('password', 0),
      errorModal('password', 1),
      confirmModal('email', 2),
      errorModal('email', 3),
      confirmModal('name', 4),
      errorModal('name', 5),
    ];
  };

  render() {
    const { user } = this.props;
    const title = `${user.name} Settings`;
    const pageTitle = 'Settings';
    return (
      <PageContainer icon="fas fa-user" title={title} pageTitle={pageTitle}>
        <Form onSubmit={this.nameFormSubmit} render={this.renderNameForm} />
        <Form onSubmit={this.emailFormSubmit} render={this.renderEmailForm} />
        <Form onSubmit={this.passwordFormSubmit} render={this.renderPasswordForm} />
        {this.modals()}
      </PageContainer>
    );
  }
}

export default Settings;
