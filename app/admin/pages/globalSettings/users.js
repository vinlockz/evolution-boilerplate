/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import React from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import PageContainer from '../../containers/PageContainer';
import Modal from '../../components/Modal';
import api from '../../lib/api';

class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: _.clone(props.users),
    };
  }

  static async getInitialProps(ctx) {
    const initialProps = {
      users: [],
    };
    if (!process.browser) {
      const { req } = ctx;
      try {
        const users = await req.db.User.find({}).exec();
        users.forEach((user) => {
          initialProps.users.push(user.toObject());
        });
      } catch (err) {}
    }
    return initialProps;
  }

  deleteUser = id => () => {
    api().delete(`/users/${id}`)
      .then(() => {
        location.reload();
      });
  };

  disableUser = id => () => {
    api().post(`/users/${id}/disable`)
      .then(() => {
        location.reload();
      });
  };

  approveUser = id => () => {
    api().post(`/users/${id}/approve`)
      .then(() => {
        location.reload();
      });
  };

  approveUserModal = (user, key) => {
    const id = `${user._id}_approve_modal`;
    const title = `Approve ${user.name}`;
    const body = `Are you sure you want to approve ${user.name} (${user.email})?`;
    const actions = [
      <button key={0} type="button" className="btn btn-dark" data-dismiss="modal">No</button>,
      <button key={1} type="button" className="btn btn-success" onClick={this.approveUser(user._id)}>Yes, Approve</button>,
    ];
    return (
      <Modal key={key} id={id} title={title} body={body} actions={actions} />
    );
  };

  deleteUserModal = (user, key) => {
    const id = `${user._id}_delete_modal`;
    const title = `Delete ${user.name}`;
    const body = `Are you sure you want to delete ${user.name} (${user.email})?`;
    const actions = [
      <button key={0} type="button" className="btn btn-dark" data-dismiss="modal">No</button>,
      <button key={1} type="button" className="btn btn-danger" onClick={this.deleteUser(user._id)}>Yes, Delete</button>,
    ];
    return (
      <Modal key={key} id={id} title={title} body={body} actions={actions} />
    );
  };

  disableUserModal = (user, key) => {
    const id = `${user._id}_disable_modal`;
    const title = `Disable ${user.name}`;
    const body = `Are you sure you want to disable ${user.name} (${user.email})?`;
    const actions = [
      <button key={0} type="button" className="btn btn-dark" data-dismiss="modal">No</button>,
      <button key={1} type="button" className="btn btn-danger" onClick={this.disableUser(user._id)}>Yes, Disable</button>,
    ];
    return (
      <Modal key={key} id={id} title={title} body={body} actions={actions} />
    );
  };

  modals = () => {
    const { users } = this.props;
    return users.map((user, key) => [
      this.deleteUserModal(user, key),
      this.approveUserModal(user, key + users.length),
      this.disableUserModal(user, key + (users.length * 2)),
    ]);
  };

  userRow = (user, key) => (
    <tr key={key}>
      <th className="text-left align-middle" scope="row">{user.name}</th>
      <td className="text-left align-middle">{user.email}</td>
      <td className="text-left align-middle">{user.description}</td>
      <td className="text-center align-middle">{user.enabled ? <i className="fas fa-check"/> : <i className="fas fa-times"/>}</td>
      <td className="text-right align-middle">
        <div className="btn-group dropleft">
          <button type="button" className="btn btn-link dropdown-toggle text-black-50" data-toggle="dropdown"
                  aria-haspopup="true" aria-expanded="false">
            <i className="far fa-ellipsis-v fa-lg"></i>
          </button>
          <div className="dropdown-menu">
            <h6 className="dropdown-header">Manage {user.name}</h6>
            <button className={classnames({ 'dropdown-item': true, 'd-none': user.enabled })} onClick={() => $(`#${user._id}_approve_modal`).modal('show')}>
              <span className="float-right">
                <i className="fas fa-check"></i>
              </span> Approve
            </button>
            <button className={classnames({ 'dropdown-item': true, 'd-none': !user.enabled })} onClick={() => $(`#${user._id}_disable_modal`).modal('show')}>
              <span className="float-right">
                <i className="fas fa-ban"></i>
              </span> Disable
            </button>
            <button className="dropdown-item text-left" type="button" onClick={() => $(`#${user._id}_delete_modal`).modal('show')}>
              <span className="float-right">
                <i className="fas fa-trash-alt"></i>
              </span> Delete
            </button>
          </div>
        </div>
      </td>
    </tr>
  );

  userList = () => {
    const { users } = this.props;
    return (
      <PageContainer title="Users" pageTitle="Users - Global Settings" icon="fas fa-users">
        <div className="row">
          <div className="col-10 offset-1">
            <table className="table">
              <thead>
                <tr>
                  <th scope="col" className="text-left">Name</th>
                  <th scope="col" className="text-left">E-Mail</th>
                  <th scope="col" className="text-left">Description</th>
                  <th scope="col" className="text-center">Enabled</th>
                  <th scope="col" className="text-right">Actions</th>
                </tr>
              </thead>
              <tbody>
                {users.map(this.userRow)}
              </tbody>
            </table>
          </div>
        </div>
        {this.modals()}
      </PageContainer>
    );
  };

  render() {
    return this.userList();
  }
}

export default Users;
