/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import React from 'react';
import _ from 'lodash';
import Modal from '../../components/Modal/index';
import api from '../../lib/api';
import PageContainer from '../../containers/PageContainer';

const SETTING_KEY = 'global.validhosts';

class ValidHosts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validHosts: _.clone(props.validHosts),
      newHosts: [],
      checkedHosts: {},
      newHostInput: '',
    };
  }

  static async getInitialProps(ctx) {
    const initialProps = {
      validHosts: [],
    };
    if (!process.browser) {
      const { req } = ctx;
      try {
        const value = await req.db.Setting.get(SETTING_KEY);
        initialProps.validHosts = value || [];
      } catch (err) {
      }
    }

    return initialProps;
  }

  componentDidMount() {
    document.getElementById('new_host')
      .addEventListener('keyup', function(event) {
        event.preventDefault();
        if (event.keyCode === 13) {
          document.getElementById('add_host').click();
        }
      });
  }

  checkHost = host => (event) => {
    const { target } = event;
    this.setState(state => ({
      checkedHosts: {
        ...state.checkedHosts,
        [host]: target.checked,
      },
    }));
  };

  listHosts = () => {
    const { validHosts, newHosts, checkedHosts } = this.state;
    const listHosts = validHosts.map((host, key) => (
      <li key={key}>
        <label>
          <input
            name={host}
            type="checkbox"
            checked={checkedHosts[host] || false}
            onChange={this.checkHost(host)}
          /> {host}
        </label>
      </li>
    ));
    const listNewHosts = newHosts.map((host, key) => (
      <li key={key} className="text-success">
        <label>
          <input
            name={host}
            type="checkbox"
            checked={checkedHosts[host] || false}
            onChange={this.checkHost(host)}
          /> {host}
        </label>
      </li>
    ));
    return (
      <ul className="list-unstyled">
        {listHosts}
        {listNewHosts}
      </ul>
    );
  };

  addHost = () => {
    const { newHostInput } = this.state;
    if (newHostInput !== '') {
      this.setState((state) => {
        const { newHosts } = state;
        newHosts.push(newHostInput);
        return { newHosts };
      }, () => {
        this.setState({
          newHostInput: '',
        });
      });
    }
  };

  toggleModal = () => {
    $('#valid_hosts_modal').modal('toggle');
  };

  toggleDeleteModal = () => {
    $('#valid_hosts_delete_modal').modal('toggle');
  };

  deleteSelected = () => {
    const { checkedHosts, validHosts, newHosts } = this.state;
    const unchecked = validHosts.concat(newHosts).filter(key => !checkedHosts[key]);
    api().post(`/settings/${SETTING_KEY}`, {
      value: unchecked,
    }).then(() => {
      location.reload();
    });
  };

  save = () => {
    const { validHosts, newHosts } = this.state;
    api().post(`/settings/${SETTING_KEY}`, {
      value: validHosts.concat(newHosts),
    }).then(() => {
      location.reload();
    });
  };

  updateNewHostInput = (event) => {
    const { target } = event;
    this.setState({
      newHostInput: target.value,
    });
  };

  render() {
    const { checkedHosts, newHosts, newHostInput } = this.state;
    const anyChecked = Object.values(checkedHosts).includes(true);
    const validHostsChanged = newHosts.length > 0;

    return (
      <PageContainer title="Valid Hosts" pageTitle="Valid Hosts - Global Settings - ZephyrQL Admin" settingKey={SETTING_KEY} icon="fas fa-server">
        <div>
          <div className="form-group container">
            <div className="mt-2 mb-2">
              <small className="text-muted">
                {/* eslint-disable-next-line max-len */}
                These host domains are used across the application for CORS and etc. (ex. google.com).<br />
                When you add hosts, they will appear in <span className="text-success">green</span>, you may then Save.<br />
                Checking hosts will allow you to select them for deletion.
              </small>
            </div>
            <h3>Hosts:</h3>
            {this.listHosts()}
            <h3>Add a host:</h3>
            <div className="input-group">
              <input onChange={this.updateNewHostInput} value={newHostInput} type="text" className="form-control" id="new_host" aria-describedby="valid_hosts_help" placeholder="eg. google.com" />
              <div className="input-group-append" id="button-addon4">
                <button id="add_host" className="btn btn-success" type="button" onClick={this.addHost} disabled={newHostInput.length === 0}>
                  <i className="fas fa-plus" />
                </button>
              </div>
            </div>
          </div>
          <button type="button" onClick={this.toggleDeleteModal} className="btn btn-danger m-2" disabled={!anyChecked}>Delete Selected</button>
          <button type="button" onClick={this.toggleModal} className="btn btn-success" disabled={!validHostsChanged}>Save</button>
          <div className="clearfix" />
          <Modal
            id="valid_hosts_modal"
            title="Save Valid Hosts"
            body="Are you sure you want to save the Valid Hosts?"
            actions={[
              <button key={0} type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>,
              <button key={1} type="button" className="btn btn-success" onClick={this.save}>Save changes</button>
            ]}
          />
          <Modal
            id="valid_hosts_delete_modal"
            title="Delete Valid Hosts"
            body={(
              <div>
                <p>Are you sure you want to delete the following hosts?</p>
                <ul>
                  {Object.keys(checkedHosts).filter((host) => {
                    return checkedHosts[host] || false;
                  }).map((host, key) => <li key={key}>{host}</li>)}
                </ul>
              </div>
            )}
            actions={[
              <button key={0} type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>,
              <button key={1} type="button" className="btn btn-danger" onClick={this.deleteSelected}>Delete Selected</button>
            ]}
          />
        </div>
      </PageContainer>
    );
  }
}

export default ValidHosts;
