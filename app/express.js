/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import http from 'http';
import express from 'express';
import helmet from 'helmet';
import routers from './routers';

/** ENVIRONMENT VARIABLES */
const {
  DISABLE_HELMET,
  DISABLE_HELMET_HSTS,
  DISABLE_HELMET_CACHE_CONTROL,
  DISABLE_HELMET_CROSS_DOMAIN,
  DISABLE_HELMET_HIDE_POWERED_BY,
} = process.env;

export default {
  /**
   * Run Express Server
   * @param {Number} [port=4000] Port
   */
  runServer: (port = 4000) => {
    /**
     * Express Application
     * @type {Function|*}
     */
    const app = express();

    /** Middleware */
    // Helmet Middleware
    if (DISABLE_HELMET !== 'true') {
      if (DISABLE_HELMET_HSTS !== 'true') {
        app.use(helmet.hsts());
      }
      if (DISABLE_HELMET_CACHE_CONTROL !== 'true') {
        app.use(helmet.noCache());
      }
      if (DISABLE_HELMET_CROSS_DOMAIN !== 'true') {
        app.use(helmet.permittedCrossDomainPolicies());
      }
      if (DISABLE_HELMET_HIDE_POWERED_BY !== 'true') {
        app.use(helmet.hidePoweredBy());
      }
    }

    /** Express Routers */
    routers(app);

    const server = http.createServer(app);
    server.listen(port, () => {
      console.log(`Application Started on Port ${port}.`);
    });
  },
};
