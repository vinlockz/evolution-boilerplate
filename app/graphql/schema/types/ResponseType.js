/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { GraphQLUnionType } from 'graphql';
import * as yup from 'yup';

/**
 * Response Type
 * @param {Object} options Options
 * @param {String} options.name Type Name
 * @param {String} options.description Type Decsription
 * @param {Object} options.responseType Response Type
 * @param {Object} options.errorType Error Type
 * @function
 */
const ResponseType = (options) => {
  const schema = yup.object().shape({
    name: yup.string().required(),
    description: yup.string().required(),
    responseType: yup.mixed().required(),
    errorType: yup.mixed().required(),
  });

  schema.validateSync(options);

  return new GraphQLUnionType({
    name: options.name,
    description: options.description,
    types: [ options.responseType, options.errorType ],
    resolveType: (value) => {
      if (Object.prototype.hasOwnProperty.call(value, 'errorCode')) {
        return options.errorType;
      }
      return options.responseType;
    }
  });
};

export default ResponseType;
