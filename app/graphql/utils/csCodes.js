/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const csCodes = {
  1000: 'CA1000',
  2000: 'CB2000',
  2001: 'CC2001',
  2002: 'CD2002',
  2100: 'CE2100',
  2101: 'CF2101',
  2102: 'CE2102',
  2104: 'CG2104',
  4002: 'CH4002',
  4904: 'CJ4904',
  7903: 'CK7903',
  7917: 'CL7917',
  7918: 'DG7918',
  '-1000': 'CM1000',
  '-1001': 'CM1001',
  '-10001': 'CN10001',
  '-10002': 'CO10002',
  '-10003': 'CP10003',
  '-10004': 'CQ10004',
  '-10005': 'CR10005',
  '-10006': 'CT10006',
  '-10007': 'CU10007',
  '-10008': 'CV10008',
  '-10009': 'CW10009',
  '-10010': 'CM10010',
  '-10011': 'CX10011',
  '-10012': 'CY10012',
  '-10013': 'CZ10013',
  '-10015': 'DA10015',
  '-10016': 'DC10016',
  '-10017': 'CP10017',
  '-10018': 'DE10018',
  '-10101': 'DF10101',
  '-10102': 'DF10102',
  '-10103': 'DF10103',
  '-10104': 'DF10104',
  '-11001': 'DF11001',
  '-11002': 'DF11002',
  '-11003': 'DF11003',
  '-11004': 'DF11004',
  '-11005': 'DF11005',
  '-11006': 'DF11006',
  '-11007': 'DF11007',
  '-11008': 'DF11008',
  '-11009': 'DF11009',
  '-11010': 'DF11010',
  '-11011': 'DF11011',
  '-11012': 'DF11012',
  '-11013': 'DF11013',
  '-11014': 'DF11014',
  '-11015': 'DF11015',
  '-11016': 'DF11016',
  '-11017': 'DF11017',
  '-11018': 'DF11018',
  '-11019': 'DF11019',
  '-11020': 'DF11020',
  '-11021': 'DF11021',
  '-11022': 'DF11022',
  '-11023': 'DF11023',
  '-11024': 'DF11024',
  '-11025': 'DF11025',
  '-11026': 'DF11026',
  '-11027': 'DF11027',
  '-11028': 'DF11028',
  '-11053': 'DF11053',
  '-11039': 'DF11039',
  '-11040': 'DF11040',
  '-11041': 'DF11041',
  '-11042': 'DF11042',
  '-11043': 'DF11043',
  '-11044': 'DF11044',
  '-11048': 'DF11048',
  default: 'NC0000',
};

export default (error) => {
  if (Object.prototype.hasOwnProperty.call(csCodes, error)) {
    return csCodes[error];
  }
  return csCodes.default;
};
