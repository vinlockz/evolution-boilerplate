/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import corsMiddleware from './corsMiddleware';
import logMiddleware from './logMiddleware';
import pmxMiddleware from './pmxMiddleware';
import redisMiddleware from './redisMiddleware';
import requestIdMiddleware from './requestIdMiddleware';
import settingsMiddleware from './settingsMiddleware';
import adminMiddleware from '../../admin/lib/ExpressMiddleware';
import Validation from '../Validation/Validation';

const {
  REDIS_ENABLED,
  REDIS_HOST,
  REDIS_PORT,
  REDIS_CLUSTER,
  REDIS_ENABLE_SSL,
} = process.env;

/**
 * Express Middleware Centralization
 * @param {Object} options Middleware Options
 * @param {Boolean} [options.pmx=false] Enable PMX Middleware
 * @param {Boolean} [options.jsonParser=false] Enable JSON Body Parser
 * @param {Boolean} [options.urlEncodedParser=false] Enable Body Parser
 * @param {Object|Boolean} [options.cookieParser=false] Enable Cookie Parser
 * @param {Boolean} [options.requestIdGenerator=false] Enable Request ID Generator
 * @param {Boolean|Object} [options.logging=false] Enable Logging
 * @param {String|null} [options.logging.name=null] Logging Options
 * @param {Boolean|Object} [options.redisCache=false] Enable Redis + Cache
 * @param {Boolean} [options.settings=false] Enable Admin Settings
 * @param {Boolean} [options.cors=false] Enable CORS
 * @param {Boolean} [options.adminDb=false] Enable Admin DB Connector
 * @returns {Array<Object>}
 */
export default (options = {}) => {
  const defaults = {
    pmx: false,
    jsonParser: false,
    urlEncodedParser: false,
    cookieParser: false,
    requestIdGenerator: false,
    logging: false,
    redisCache: false,
    settings: false,
    cors: false,
    adminDb: false,
  };

  options = { ...defaults, ...options };

  const middleware = [];

  // PMX Middleware
  if (options.pmx) middleware.push(pmxMiddleware());

  // JSON Body Parser
  if (options.jsonParser) middleware.push(bodyParser.json());

  // URL Encoded Body Parser
  if (options.urlEncodedParser) middleware.push(bodyParser.urlencoded({ extended: true }));

  // Cookie Parser
  if (options.cookieParser) {
    if (Validation.isObject(options.cookieParser)) {
      middleware.push(cookieParser(options.cookieParser));
    } else {
      middleware.push(cookieParser());
    }
  }

  // Request ID Generator
  if (options.requestIdGenerator) middleware.push(requestIdMiddleware());

  // Logging Middleware Instance
  if (options.logging) {
    if (Validation.isString(options.logging)) {
      middleware.push(logMiddleware(options.logging));
    } else {
      middleware.push(logMiddleware());
    }
  }

  // Redis Connection / Cache Functionality
  if (options.redisCache) {
    middleware.push(redisMiddleware({
      redisEnabled: REDIS_ENABLED === 'true',
      redisHost: REDIS_HOST || null,
      redisPort: REDIS_PORT || 6379,
      redisCluster: REDIS_CLUSTER === 'true',
      redisEnableSSL: REDIS_ENABLE_SSL === 'true',
      redisAdditionalConfig: Validation.isObject(options.redisCache) ? options.redisCache : {},
    }));
  }

  if (options.adminDb) middleware.push(adminMiddleware({ adminDb: 'adminDb' }));

  // Admin Settings Middleware
  if (options.settings) middleware.push(settingsMiddleware());

  // CORS Middleware
  if (options.cors) {
    if (!options.settings) throw new Error('CORS is dependent on Settings Middleware.');
    middleware.push(corsMiddleware());
  }

  return middleware;
};
