/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import Logger from '../Logger/Logger';
import getIpAddress from '../../graphql/utils/getIpAddress';

/* todo: this needs documentation */

/**
 * Logs the Server Request ID
 * @param request
 */
const logServerRequestId = (request) => {
  if (Object.prototype.hasOwnProperty.call(request, 'serverRequestId')) {
    request.logger.addMetadata('serverRequestId', request.serverRequestId);
  }
};

/**
 * Logs the Express Request
 * @param request
 */
const logRequest = (request) => {
  if (Object.prototype.hasOwnProperty.call(request.headers, 'x-request-id')) {
    request.logger.addMetadata('clientRequestId', request.headers['x-request-id']);
  }

  request.logger.log('client.request', {
    headers: request.headers,
    method: request.method,
    params: request.params,
    query: request.query,
    body: request.body,
    path: request.path,
    signedCookies: request.signedCookies,
    originalUrl: request.originalUrl,
    protocol: request.protocol,
    baseUrl: request.baseUrl,
    ip: getIpAddress(request),
    route: request.route,
    secure: request.secure,
  });
};

/**
 * Logs the Express Response
 * @param request
 * @param response
 */
const logResponse = (request, response) => {
  // On finish, commit logs
  response.on('finish', () => {
    request.logger.log('client.response', {
      body: response.body,
      headers: response.getHeaders(),
      status: response.statusCode,
    });
  });

  const oldWrite = response.write;
  const oldEnd = response.end;
  const chunks = [];

  // Proxy res.write
  response.write = function (...args) {
    chunks.push(Buffer.from(args[0]));
    oldWrite.apply(response, args);
  };

  // Proxy res.end
  response.end = function(...args) {
    if (args[0]) {
      chunks.push(Buffer.from(args[0]));
    }
    const body = Buffer.concat(chunks).toString('utf8');
    let jsonBody = null;
    try {
      jsonBody = JSON.parse(body);
    } catch (err) {
      jsonBody = body || null;
    }

    response.body = jsonBody;
    oldEnd.apply(response, args);
  };
};

/**
 * IF buGSNAG
 * @param request
 */
const bugsnagLogging = (request) => {
  if (Object.hasOwnProperty.call(request, 'bugsnag')) {
    request.bugsnag.config.beforeSend.push((report) => {
      request.logger.log('bugsnag.report', report);
    });
  }
};

const logMiddleware = (name = null) => (req, res, next) => {
  // Get and set logger
  req.logger = new Logger({ name });

  // Log Request ID to Log MetaData
  logServerRequestId(req);

  // Log Request
  logRequest(req);

  // On Complete
  logResponse(req, res);

  // Bugsnag logging
  bugsnagLogging(req);

  return next();
};

export default logMiddleware;
