/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import winston from 'winston';
import stringify from 'json-stringify-safe';
import CloudWatchTransport from 'winston-aws-cloudwatch';
import redact from '../RedactObject';
import os from 'os';

// ENVIRONMENT VARIABLES
const {
  LOGGING_CLOUDWATCH_ENABLE,
  LOGGING_CLOUDWATCH_LOG_GROUP_NAME,
  AWS_REGION,
  DEBUG_MODE,
  REDACT_KEYS,
  REDACT_DISABLE,
  REDACT_REPLACE,
} = process.env;
let { LOGGING_CLOUDWATCH_LOG_STREAM_NAME } = process.env;

if (!LOGGING_CLOUDWATCH_LOG_STREAM_NAME || LOGGING_CLOUDWATCH_LOG_STREAM_NAME === '') {
  LOGGING_CLOUDWATCH_LOG_STREAM_NAME = os.hostname();
}

let redactKeys = [];
if (REDACT_DISABLE !== 'true' && Boolean(REDACT_KEYS)) {
  redactKeys = REDACT_KEYS.split(',');
}

/**
 * Logger Class
 */
class Logger {
  /**
   * CloudWatch Transport for Winston
   * @returns {CloudWatchTransport}
   * @function
   * @private
   */
  _cloudWatchTransport = () => {
    if (!LOGGING_CLOUDWATCH_LOG_GROUP_NAME) {
      throw new Error('LOGGING_CLOUDWATCH_LOG_GROUP_NAME is undefined. It must be defined if CloudWatch is enabled.');
    }
    if (!LOGGING_CLOUDWATCH_LOG_STREAM_NAME) {
      throw new Error('LOGGING_CLOUDWATCH_LOG_STREAM_NAME is undefined. It must be defined if CloudWatch is enabled.');
    }
    return new CloudWatchTransport({
      logGroupName: LOGGING_CLOUDWATCH_LOG_GROUP_NAME,
      logStreamName: LOGGING_CLOUDWATCH_LOG_STREAM_NAME,
      createLogGroup: true,
      createLogStream: true,
      formatLog: this._logFormat(),
      awsConfig: {
        region: AWS_REGION,
      },
    });
  };

  /**
   * Log Format Function for Winston
   * @returns {function(*): string}
   * @private
   * @function
   */
  _logFormat = () => (info) => {
    let { message } = info;
    if (typeof message === 'object') {
      message = redact(stringify(message), {
        keys: redactKeys,
        replace: (REDACT_REPLACE !== '' && REDACT_REPLACE !== undefined && REDACT_REPLACE !== null && REDACT_REPLACE.length > 0) ? REDACT_REPLACE : undefined,
      });
    }
    let formattedMessage = `${info.level}: ${message}`;
    if (this.name !== null) {
      formattedMessage = `${this.name} - `.concat(formattedMessage)
    }
    return formattedMessage;
  };

  /**
   * Update the Log Name
   * @param name
   * @function
   * @public
   */
  updateName = (name) => {
    this.name = name;
  };

  /**
   * Logger Constructor
   * @param {Object} options Logger Options
   * @param {String|null} [options.name=null] Logger Default Name
   * @param {Object} [options.metadata={}] Logger Default MetaData
   * @param {Array<function>} [options.transports=[]] Logger Winston Transports
   * @param {Boolean} [options.disableConsoleLog=false] Disable Console Log Transport in Winston
   * @constructor
   */
  constructor(options) {
    const defaults = {
      name: null,
      metadata: {},
      transports: [],
      disableConsoleLog: false,
    };
    options = Object.assign(defaults, options);
    this.debugMode = DEBUG_MODE === 'true';
    this.name = options.name;
    this.metadata = Object.assign(defaults.metadata, options.metadata);

    // Console Log Transport
    if (!options.disableConsoleLog) {
      const consoleTransport = new winston.transports.Console();
      if (!options.transports.includes(consoleTransport)) {
        options.transports.push(consoleTransport);
      }
    }

    this.winston = winston.createLogger({
      level: 'info',
      format: winston.format.combine(winston.format.printf(this._logFormat())),
      transports: [
        ...options.transports,
      ],
    });

    if (LOGGING_CLOUDWATCH_ENABLE === 'true') {
      this.winston.add(this._cloudWatchTransport());
    }
  }

  /**
   * Add Metadata
   * @param {String} key Metadata Key
   * @param {*} value Metadata Value
   * @returns {Logger}
   * @public
   * @function
   */
  addMetadata = (key, value) => {
    if (typeof key === 'object') {
      this.metadata = Object.assign(this.metadata, key);
    } else {
      this.metadata[String(key)] = value;
    }
    return this;
  };

  /**
   * Remove Metadata
   * @param {String} key Metadata Key
   * @returns {Logger}
   * @public
   * @function
   */
  removeMetaData = (key) => {
    delete this.metadata[String(key)];
    return this;
  };

  /**
   * Message Format
   * @param {String} type Message Type
   * @param {*} message Message
   * @returns {{name: String, type: String, message: *, meta: *}}
   * @private
   * @function
   */
  _messageFormat = (type, message) => ({
    name: this.name,
    type,
    message: message,
    meta: this.metadata,
  });

  /**
   * Log Info
   * @param {String} type Log Type
   * @param {String} message Log Message
   * @returns {winston.Logger}
   * @public
   * @function
   */
  log = (type, message) => this.winston.log({
    level: 'info',
    message: this._messageFormat(type, message),
  });

  /**
   * Log Info
   * @param {String} type Log Type
   * @param {String} message Log Message
   * @returns {winston.Logger}
   * @public
   * @function
   */
  info = this.log;

  /**
   * Log Warn
   * @param {String} type Log Type
   * @param {String} message Log Message
   * @returns {winston.Logger}
   * @public
   * @function
   */
  warn = (type, message) => this.winston.log({
    level: 'warn',
    message: this._messageFormat(type, message),
  });

  /**
   * Log Error
   * @param {String} type Log Type
   * @param {String} message Log Message
   * @returns {winston.Logger}
   * @public
   * @function
   */
  error = (type, message) => this.winston.log({
    level: 'error',
    message: this._messageFormat(type, message),
  });

  /**
   * Log Error
   * @param {String} type Log Type
   * @param {String} message Log Message
   * @returns {winston.Logger}
   * @public
   * @function
   */
  err = this.error;

  /**
   * Log Debug Info (Only if DebugMode is on)
   * @param {String} type Log Type
   * @param {String} message Log Message
   * @returns {winston.Logger}
   * @public
   * @function
   */
  debug = (type, message) => {
    if (this.debugMode) {
      this.winston.log({
        level: 'info',
        message: this._messageFormat(type, message),
      });
    }
  };

  /**
   * Retrieve Winston Instance
   * @returns {winston.Logger | *}
   */
  getLogger = () => this.winston;
}

export default Logger;
