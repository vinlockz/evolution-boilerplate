/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import Validation from "../Validation/Validation";

/**
 * Redact Variables
 * @param {*} element
 * @param {Object} options Options
 * @param {Boolean} [options.enabled=true] Redaction Enabled
 * @param {String} [options.replace="[REDACTED]"] String to replace with.
 * @param {Array} options.keys Object Keys to redact.
 * @returns {*}
 */
const redact = (element, options = {}) => {
  options = Object.assign({
    enabled: true,
    replace: '[REDACTED]',
    keys: [],
  }, options);
  if (options.enabled && options.keys.length) {
    // JSON Check
    let json = null;
    if (Validation.isString(element)) {
      try {
        json = JSON.parse(element);
      } catch (e) {
        json = null;
      }
    }
    if (json) return JSON.stringify(redact(element, options));

    // Object Check
    if (Validation.isObject(element)) {
      const objectKeys = Object.keys(element);
      objectKeys.forEach(function (key) {
        if (options.keys.indexOf(key) !== -1) {
          element[ key ] = options.replace;
        } else {
          element[ key ] = redact(element[ key ], options);
        }
      });
      return element;
    }

    // Array Check
    if (Validation.isArray(element)) {
      element = element.map(arrayElement => redact(arrayElement, options));
    }
  }

  return element;
};

export default redact;
