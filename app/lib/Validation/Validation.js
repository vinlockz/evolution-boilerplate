/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const isObject = ob => ob && typeof ob === 'object' && ob.constructor === Object;

const isFunction = func => typeof func === 'function';

const isNull = nullValue => nullValue === null;

const isUndefined = und => typeof und === 'undefined';

const isBoolean = bool => typeof bool === 'boolean';

const isArray = arr => arr && typeof arr === 'object' && arr.constructor === Array;

const isNumber = num => typeof num === 'number' && isFinite(num);

const isString = str => typeof str === 'string' || str instanceof String;

const isJson = (str) => {
  if (!isString(str)) return false;
  let json = null;
  try {
    json = JSON.parse(str);
  } catch (e) {
    return false;
  }
  return json;
};

const Validation = {
  isObject,
  isFunction,
  isNull,
  isUndefined,
  isBoolean,
  isArray,
  isNumber,
  isString,
  isJson,
};

export default Validation;
