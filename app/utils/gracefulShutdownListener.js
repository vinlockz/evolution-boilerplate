/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import Logger from '../lib/Logger/Logger';

const connections = [];

const shutdown = (server) => () => {
  const logger = new Logger({ name: 'service' });
  logger.warn('server.warn', 'Received kill signal, shutting down gracefully.');
  server.on('close', () => {
    logger.log('server.notice', {
      message: 'Closed out connections',
    });
    process.exit(0);
  });

  setTimeout(() => {
    logger.error('server.error', {
      message: 'Could not close out connections in time, forcefully shutting down.',
    });
    process.exit(1);
  }, 10000);

  connections.forEach(curr => curr.end());
  setTimeout(() => {
    connections.forEach(curr => curr.destroy())
  }, 5000);
};

const gracefulShutdownListener = (server) => {
  // Manage Connections
  server.on('connection', (connection) => {
    connections.push(connection);
    connection.on('close', () => connections.filter(curr => curr !== connection));
  });

  // Shutdown gracefully
  process.on('SIGTERM', shutdown(server));
  process.on('SIGINT', shutdown(server));
};

export default gracefulShutdownListener;
